from door import SmartDoor
from gate import SmartGate
from Lamp import SmartLamp
from TempSensor import TempSensor
from window import SmartWindow
from main import App
import sys
from PyQt5.QtWidgets import QApplication

app = QApplication(sys.argv)
ex = App(True)

window = SmartWindow("window1", True)
window.start()

gate = SmartGate("gate1", True)
gate.start()

lamp = SmartLamp("lamp1", True)
lamp.start()

my_temp_sensor = TempSensor("my_temp_sensor", True)
my_temp_sensor.start()

door = SmartDoor("door1", True)
door.start()

app.exec_()