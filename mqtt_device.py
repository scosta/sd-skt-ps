#!/usr/bin/python3

import paho.mqtt.client as mqtt
import time
from PyQt5.QtCore import QObject, Qt, pyqtSignal


class MqttDevice(QObject):

    client = None

    # create signal
    message_received = pyqtSignal(str, str, name='mqtt_new_msg')

    def __init__(self, on_message=None):

        super().__init__()
        self.client = mqtt.Client()
        if on_message is None:
            self.client.on_message = self.on_message
        else:
            self.client.on_message = on_message
        self.client.on_connect = self.on_connect
        #self.client.connect("iot.eclipse.org", 1883, 60)
        self.client.connect("localhost", 1883, 60)
        self.client.loop_start()

        # # create signal
        # self.message_received = pyqtSignal(str, str, name='mqtt_new_msg')

    def on_connect(self, client, userdata, flags, rc):
        print("MqttDevice connected with result code {}".format(str(rc)))

    def on_message(self, client, userdata, message):
        print("message received ", str(message.payload.decode("utf-8")))
        print("message topic = ", message.topic)
        print("message qos = ", message.qos)
        print("message retain flag = ", message.retain)
        self.message_received.emit(str(message.topic), str(message.payload.decode("utf-8")))

    def publish(self, topic, message):
        self.client.publish(topic, message)

    def stop(self):
        self.client.loop_stop()

    def subscribe(self, topic):
        self.client.subscribe(topic)
