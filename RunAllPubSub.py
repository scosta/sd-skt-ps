from door import SmartDoor
from gate import SmartGate
from Lamp import SmartLamp
from TempSensor import TempSensor
from window import SmartWindow
from main import App
import sys
from PyQt5.QtWidgets import QApplication

window = SmartWindow("window1", False)
window.start()

gate = SmartGate("gate1", False)
gate.start()

lamp = SmartLamp("lamp1", False)
lamp.start()

my_temp_sensor = TempSensor("my_temp_sensor", False)
my_temp_sensor.start()

door = SmartDoor("door1", False)
door.start()

app = QApplication(sys.argv)
ex = App(False)
sys.exit(app.exec_())