import random
from mqtt_device import MqttDevice
from socket_device import SocketDevice
from threading import Thread
import time
from PyQt5.QtCore import QObject, Qt, pyqtSignal
from PyQt5.QtCore import pyqtSlot


class SmartWindow(Thread, QObject):

    window_status = "CLOSED"
    name = ""
    use_socket = False
    mqtt_device = None
    socket_device = None

    mqtt_topic = "sd2019.1-mqtt/smart-window"
    cmd_mqtt_topic = "sd2019.1-mqtt/smart-window-cmd"

    def __init__(self, name, use_socket=False):
        Thread.__init__(self)
        QObject.__init__(self)
        self.name = name
        self.use_socket = use_socket
        self.dev_id = "smart_window"
        if self.use_socket:
            self.socket_device = SocketDevice(self.dev_id)
        else:
            self.mqtt_device = MqttDevice(self.on_message)
            self.mqtt_device.message_received.connect(self.update_window_status)
            self.mqtt_device.subscribe(self.cmd_mqtt_topic)

    def status(self):
        return self.window_status

    def on_message(self, client, userdata, message):
        print("message received ", str(message.payload.decode("utf-8")))
        print("message topic = ", message.topic)
        print("message qos = ", message.qos)
        print("message retain flag = ", message.retain)
        self.window_status = str(message.payload.decode("utf-8"))

    def process_inbox(self, msg):
        msg_list = msg.split("\n")
        for m in msg_list:
            if m == "" or m is None:
                continue
            if msg.find(self.cmd_mqtt_topic) >= 0:
                new_value = msg.split("%")[1]
                print("update_window_status: " + new_value)
                self.window_status = new_value

    @pyqtSlot(str, str)
    def update_window_status(self, key, value):
        print("update_window_status: " + key + " " + value)
        if key == self.cmd_mqtt_topic:
            self.window_status = value

    def run(self):
        while True:
            if self.use_socket:
                self.process_inbox(self.socket_device.inbox())
                self.socket_device.send_message(self.mqtt_topic + "%" + self.status())
            else:
                self.mqtt_device.publish(self.mqtt_topic, self.status())
            time.sleep(5)

#
# if __name__ == "__main__":
#     door = SmartWindow("window1", True)
#     door.start()

