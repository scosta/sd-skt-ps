import random
from mqtt_device import MqttDevice
from socket_device import SocketDevice
from threading import Thread
import time
from PyQt5.QtCore import QObject, Qt, pyqtSignal
from PyQt5.QtCore import pyqtSlot


class SmartLamp(Thread, QObject):

    lamp_status = "OFF"
    name = ""
    use_socket = False
    mqtt_device = None
    socket_device = None

    mqtt_topic = "sd2019.1-mqtt/smart-lamp"
    cmd_mqtt_topic = "sd2019.1-mqtt/smart-lamp-cmd"

    def __init__(self, name, use_socket=False):
        Thread.__init__(self)
        QObject.__init__(self)
        self.name = name
        self.use_socket = use_socket
        self.dev_id = "smart_lamp"
        if self.use_socket:
            self.socket_device = SocketDevice(self.dev_id)
        else:
            self.mqtt_device = MqttDevice(self.on_message)
            self.mqtt_device.message_received.connect(self.update_lamp_status)
            self.mqtt_device.subscribe(self.cmd_mqtt_topic)

    def status(self):
        return self.lamp_status

    def on_message(self, client, userdata, message):
        print("message received ", str(message.payload.decode("utf-8")))
        print("message topic = ", message.topic)
        print("message qos = ", message.qos)
        print("message retain flag = ", message.retain)
        self.lamp_status = str(message.payload.decode("utf-8"))

    def process_inbox(self, msg):
        msg_list = msg.split("\n")
        for m in msg_list:
            if m == "":
                continue
            if msg.find(self.cmd_mqtt_topic) >= 0:
                new_value = msg.split("%")[1]
                print("update_lamp_status: " + new_value)
                self.lamp_status = new_value

    @pyqtSlot(str, str)
    def update_lamp_status(self, key, value):
        print("update_lamp_status: " + key + " " + value)
        if key == self.cmd_mqtt_topic:
            self.lamp_status = value

    def run(self):
        while True:
            if self.use_socket:
                self.process_inbox(self.socket_device.inbox())
                self.socket_device.send_message(self.mqtt_topic + "%" + self.status())
            else:
                self.mqtt_device.publish(self.mqtt_topic, self.status())
            time.sleep(5)


# if __name__ == "__main__":
#     lamp = SmartLamp("lamp1", True)
#     lamp.start()

