#!/usr/bin/python3

import socket
from threading import Thread
from PyQt5.QtCore import QObject, Qt, pyqtSignal, pyqtSlot


class ClientThread(Thread, QObject):

    # create signal
    message_received = pyqtSignal(str, str, name='ClientThread')
    register_dev_id = pyqtSignal(str, name='register_dev_id')

    def __init__(self, clientAddress, clientsocket):
        Thread.__init__(self)
        QObject.__init__(self)
        self.clientAddress = clientAddress
        self.clientsocket = clientsocket
        self.csocket = clientsocket
        self.csocket.setblocking(1)
        print("New connection added: ", self.clientAddress)

    def send_message(self, msg):
        print("ClientThread Broadcast: v: {}".format(msg))
        self.csocket.send(bytes(msg+"\n", 'UTF-8'))

    def run(self):
        print("Connection from : ", self.clientAddress)
        while True:
            (data, sender_addr) = self.csocket.recvfrom(1024)
            if len(data) != 0:
                msg = data.decode()
                #print(str(self.clientAddress) + ": " + str(msg))
                msg_list = msg.split("\n")
                for m in msg_list:
                    if m == "":
                        continue
                    if m.find("id:") >= 0:
                        aux = m.split(":")
                        device_id = aux[1]

                    else:
                        self.message_received.emit(str(self.clientAddress), m)


class SocketServer(Thread, QObject):

    # create signal
    message_received = pyqtSignal(str, str, name='socket_server_msg')
    thread_list = []

    def __init__(self):
        Thread.__init__(self)
        QObject.__init__(self)
        self.client_map = {}
        self.LOCALHOST = "127.0.0.1"
        self.PORT = 8765
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((self.LOCALHOST, self.PORT))

    @pyqtSlot(str, str)
    def update_sensor_value(self, key, value):
        self.message_received.emit(key, value)


    def send_message(self, cmd_mqtt_topic, value):
        print("Broadcast: t: {} v: {}".format(cmd_mqtt_topic, value))
        for t in self.thread_list:
            t.send_message(cmd_mqtt_topic+"%"+value)
        # if cmd_mqtt_topic == self.lamp_cmd_mqtt_topic:
        #     self.thread_map[self.lamp_cmd_mqtt_topic].send_message(value)
        # elif cmd_mqtt_topic == self.door_cmd_mqtt_topic:
        #     self.thread_map[self.door_cmd_mqtt_topic].send_message(value)
        # elif cmd_mqtt_topic == self.gate_cmd_mqtt_topic:
        #     self.thread_map[self.gate_cmd_mqtt_topic].send_message(value)
        # elif cmd_mqtt_topic == self.window_cmd_mqtt_topic:
        #     self.thread_map[self.window_cmd_mqtt_topic].send_message(value)
        # else:
        #     print("[ERROR]")

    def run(self) -> None:
        while True:
            self.server.listen(1)
            clientsock, clientAddress = self.server.accept()
            newthread = ClientThread(clientAddress, clientsock)
            self.thread_list.append(newthread)
            newthread.message_received.connect(self.update_sensor_value)
            newthread.start()

# if __name__ == "__main__":
#     controller = SocketServer()
#     controller.start()