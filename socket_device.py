#!/usr/bin/python3           # This is client.py file

import socket
import time
import random
from PyQt5.QtCore import QObject, Qt, pyqtSignal


class SocketDevice(QObject):

    SERVER = "127.0.0.1"
    PORT = 8765

    # create signal
    message_received = pyqtSignal(str, name='socket_new_msg_from_controller')

    def __init__(self, device_id):
        QObject.__init__(self)
        self.device_id = device_id
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((self.SERVER, self.PORT))
        self.client.sendall(bytes("id:"+self.device_id+"\n", 'UTF-8'))

    def send_message(self, message):
        self.client.sendall(bytes(message+"\n", 'UTF-8'))

    def inbox(self):
        in_data = self.client.recv(1024)
        return in_data.decode()

    def stop(self):
        self.client.close()


# if __name__ == "__main__":
#     controller1 = SocketDevice()
#     controller2 = SocketDevice()
#     while True:
#         controller1.send_message("client " + str(random.randint(0, 100)))
#         time.sleep(1)
#         controller2.send_message("client " + str(random.randint(0, 100)))
#         time.sleep(10)
