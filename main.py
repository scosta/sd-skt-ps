import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget, QTableWidgetItem, QVBoxLayout, QHeaderView, QPushButton, QDesktopWidget
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
from Controller import Controller


class App(QWidget):

    def __init__(self, use_socket=False):
        super().__init__()
        self.title = 'Smart House'
        self.left = 0
        self.top = 0
        self.width = 400
        self.height = 200
        self.initUI()
        self.controller = Controller(use_socket)
        self.controller.new_lamp_value.connect(self.update_lamp_value)
        self.controller.new_temp_value.connect(self.update_sensor_value)
        self.controller.new_door_value.connect(self.update_door_value)
        self.controller.new_gate_value.connect(self.update_gate_value)
        self.controller.new_window_value.connect(self.update_window_value)
        self.controller.start()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        qtRectangle = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        qtRectangle.moveCenter(centerPoint)
        self.move(qtRectangle.topLeft())

        self.createTable()

        # Add box layout, add table to box layout and add box layout to widget
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tableWidget)
        self.setLayout(self.layout)

        # Show widget
        self.show()

    def createTable(self):
        # Create table
        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(5)
        self.tableWidget.setColumnCount(3)
        self.tableWidget.setItem(0, 0, QTableWidgetItem("Lamp"))
        self.tableWidget.setItem(0, 1, QTableWidgetItem("OFF"))

        self.lamp_btn = QPushButton(self.tableWidget)
        self.lamp_btn.clicked.connect(self.lamp_click)
        self.lamp_btn.setText('Switch')
        self.tableWidget.setCellWidget(0, 2, self.lamp_btn)

        self.tableWidget.setItem(1, 0, QTableWidgetItem("Temperature Sensor"))
        self.tableWidget.setItem(1, 1, QTableWidgetItem("30 C"))

        # self.temp_btn = QPushButton(self.tableWidget)
        # self.temp_btn.setText('Switch')
        # self.tableWidget.setCellWidget(1, 2, self.temp_btn)

        self.tableWidget.setItem(2, 0, QTableWidgetItem("Door"))
        self.tableWidget.setItem(2, 1, QTableWidgetItem("CLOSED"))

        self.door_btn = QPushButton(self.tableWidget)
        self.door_btn.clicked.connect(self.door_click)
        self.door_btn.setText('Switch')
        self.tableWidget.setCellWidget(2, 2, self.door_btn)

        self.tableWidget.setItem(3, 0, QTableWidgetItem("Gate"))
        self.tableWidget.setItem(3, 1, QTableWidgetItem("CLOSED"))

        self.gate_btn = QPushButton(self.tableWidget)
        self.gate_btn.clicked.connect(self.gate_click)
        self.gate_btn.setText('Switch')
        self.tableWidget.setCellWidget(3, 2, self.gate_btn)

        self.tableWidget.setItem(4, 0, QTableWidgetItem("Window"))
        self.tableWidget.setItem(4, 1, QTableWidgetItem("CLOSED"))

        self.window_btn = QPushButton(self.tableWidget)
        self.window_btn.clicked.connect(self.window_click)
        self.window_btn.setText('Switch')
        self.tableWidget.setCellWidget(4, 2, self.window_btn)


        # Set the table headers
        self.tableWidget.setHorizontalHeaderLabels(["Device", "Status", "Action"])
        header = self.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, QHeaderView.ResizeToContents)


    @pyqtSlot()
    def lamp_click(self):
        print("lamp_click")
        if self.tableWidget.item(0, 1).text() == "ON":
            self.controller.send_cmd_lamp("OFF")
        else:
            self.controller.send_cmd_lamp("ON")

    @pyqtSlot()
    def door_click(self):
        print("door_click")
        if self.tableWidget.item(2, 1).text() == "OPEN":
            self.controller.send_cmd_door("CLOSED")
        else:
            self.controller.send_cmd_door("OPEN")

    @pyqtSlot()
    def gate_click(self):
        print("gate_click")
        if self.tableWidget.item(3, 1).text() == "OPEN":
            self.controller.send_cmd_gate("CLOSED")
        else:
            self.controller.send_cmd_gate("OPEN")

    @pyqtSlot()
    def window_click(self):
        print("window_click")
        if self.tableWidget.item(4, 1).text() == "OPEN":
            self.controller.send_cmd_window("CLOSED")
        else:
            self.controller.send_cmd_window("OPEN")

    @pyqtSlot(str)
    def update_lamp_value(self, value):
        self.tableWidget.item(0, 1).setText(value)

    @pyqtSlot(str)
    def update_sensor_value(self, value):
        self.tableWidget.item(1, 1).setText(value)

    @pyqtSlot(str)
    def update_door_value(self, value):
        self.tableWidget.item(2, 1).setText(value)

    @pyqtSlot(str)
    def update_gate_value(self, value):
        self.tableWidget.item(3, 1).setText(value)

    @pyqtSlot(str)
    def update_window_value(self, value):
        self.tableWidget.item(4, 1).setText(value)



# if __name__ == '__main__':
#     app = QApplication(sys.argv)
#     ex = App()
#     sys.exit(app.exec_())
