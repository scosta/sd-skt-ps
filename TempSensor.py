import random
from mqtt_device import MqttDevice
from socket_device import SocketDevice
from threading import Thread
import time
from PyQt5.QtCore import QObject, Qt, pyqtSignal
from PyQt5.QtCore import pyqtSlot


class TempSensor(Thread, QObject):

    use_socket = False
    mqtt_device = None
    socket_device = None
    mqtt_topic = "sd2019.1-mqtt/temp-sensor"

    def __init__(self, name, use_socket=False):
        Thread.__init__(self)
        QObject.__init__(self)
        self.name = name
        self.use_socket = use_socket
        self.dev_id = "smart_temp_sensor"
        if self.use_socket:
            self.socket_device = SocketDevice(self.dev_id)
        else:
            self.mqtt_device = MqttDevice()

    def run(self):

        while True:
            if self.use_socket:
                self.socket_device.send_message(self.mqtt_topic + "%" + self.status())
            else:
                self.mqtt_device.publish(self.mqtt_topic, self.status())
            time.sleep(5)

    @staticmethod
    def status():
        return str(random.randint(25, 31))


# if __name__ == "__main__":
#     my_temp_sensor = TempSensor("my_temp_sensor", True)
#     my_temp_sensor.start()
