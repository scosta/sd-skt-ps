from TempSensor import TempSensor
from mqtt_device import MqttDevice
from threading import Thread
import time
from socket_controller import SocketServer
from PyQt5.QtCore import QObject, Qt, pyqtSignal
from PyQt5.QtCore import pyqtSlot


class Controller(Thread, QObject):

    temp_sensor = None
    mqtt_device = None
    socket_server = None
    use_socket = False

    temp_sensor_mqtt_topic = "sd2019.1-mqtt/temp-sensor"
    smart_lamp_mqtt_topic = "sd2019.1-mqtt/smart-lamp"
    smart_door_mqtt_topic = "sd2019.1-mqtt/smart-door"
    smart_gate_mqtt_topic = "sd2019.1-mqtt/smart-gate"
    smart_window_mqtt_topic = "sd2019.1-mqtt/smart-window"

    lamp_cmd_mqtt_topic = "sd2019.1-mqtt/smart-lamp-cmd"
    door_cmd_mqtt_topic = "sd2019.1-mqtt/smart-door-cmd"
    gate_cmd_mqtt_topic = "sd2019.1-mqtt/smart-gate-cmd"
    window_cmd_mqtt_topic = "sd2019.1-mqtt/smart-window-cmd"

    # create signal
    new_lamp_value = pyqtSignal(str, name='new_lamp_value')
    new_temp_value = pyqtSignal(str, name='new_temp_value')
    new_door_value = pyqtSignal(str, name='new_door_value')
    new_gate_value = pyqtSignal(str, name='new_gate_value')
    new_window_value = pyqtSignal(str, name='new_window_value')

    def __init__(self, use_socket=False):
        Thread.__init__(self)
        QObject.__init__(self)
        self.use_socket = use_socket

        if self.use_socket:
            self.socket_server = SocketServer()
            self.socket_server.message_received.connect(self.update_sensor_value_via_socket)
        else:

            # self.sensor_value_map[self.temp_sensor_mqtt_topic] = ""
            # self.sensor_value_map[self.smart_lamp_mqtt_topic] = ""

            self.mqtt_device = MqttDevice()
            self.mqtt_device.message_received.connect(self.update_sensor_value)
            self.mqtt_device.subscribe(self.temp_sensor_mqtt_topic)
            self.mqtt_device.subscribe(self.smart_lamp_mqtt_topic)
            self.mqtt_device.subscribe(self.smart_door_mqtt_topic)
            self.mqtt_device.subscribe(self.smart_gate_mqtt_topic)
            self.mqtt_device.subscribe(self.smart_window_mqtt_topic)

    @pyqtSlot(str, str)
    def update_sensor_value(self, key, value):
        if key == self.temp_sensor_mqtt_topic:
            self.new_temp_value.emit(value)
        elif key == self.smart_lamp_mqtt_topic:
            self.new_lamp_value.emit(value)
        elif key == self.smart_door_mqtt_topic:
            self.new_door_value.emit(value)
        elif key == self.smart_gate_mqtt_topic:
            self.new_gate_value.emit(value)
        elif key == self.smart_window_mqtt_topic:
            self.new_window_value.emit(value)
        else:
            print("[ERROR]")


    @pyqtSlot(str, str)
    def update_sensor_value_via_socket(self, key, value):
        try:
            print("update_sensor_value_via_socket: v: {}".format(value))
            (sensor, message) = value.split("%")
            print("update_sensor_value_via_socket: k: {} -> v: {}".format(sensor, message))
            if sensor == self.temp_sensor_mqtt_topic:
                self.new_temp_value.emit(message)
            elif sensor == self.smart_lamp_mqtt_topic:
                self.new_lamp_value.emit(message)
            elif sensor == self.smart_door_mqtt_topic:
                self.new_door_value.emit(message)
            elif sensor == self.smart_gate_mqtt_topic:
                self.new_gate_value.emit(message)
            elif sensor == self.smart_window_mqtt_topic:
                self.new_window_value.emit(message)
            else:
                print("[ERROR]")
        except:
            print("[WARNING]")

    def send_cmd_lamp(self, value):
        if self.use_socket:
            self.socket_server.send_message(self.lamp_cmd_mqtt_topic, value)
        else:
            self.mqtt_device.publish(self.lamp_cmd_mqtt_topic, value)

    def send_cmd_door(self, value):
        if self.use_socket:
            self.socket_server.send_message(self.door_cmd_mqtt_topic, value)
        else:
            self.mqtt_device.publish(self.door_cmd_mqtt_topic, value)

    def send_cmd_gate(self, value):
        if self.use_socket:
            self.socket_server.send_message(self.gate_cmd_mqtt_topic, value)
        else:
            self.mqtt_device.publish(self.gate_cmd_mqtt_topic, value)

    def send_cmd_window(self, value):
        if self.use_socket:
            self.socket_server.send_message(self.window_cmd_mqtt_topic, value)
        else:
            self.mqtt_device.publish(self.window_cmd_mqtt_topic, value)

    def run(self):
        self.socket_server.start()
        while True:
            time.sleep(5)


# if __name__ == "__main__":
#     controller = Controller()
#     controller.start()